# standard imports
import unittest

# local imports
from jsonrpc_std.base import JSONRPCBase
from jsonrpc_std.interface import (
        jsonrpc_request,
        jsonrpc_response,
        jsonrpc_result,
        jsonrpc_is_response_to,
        )


class TestRequest(unittest.TestCase):

    def test_request(self):
        req = jsonrpc_request('foo_barBaz')
        self.assertEqual(req['jsonrpc'], JSONRPCBase.version_string)
        self.assertEqual(type(req['id']).__name__, 'str')
        self.assertEqual(req['method'], 'foo_barBaz')
        self.assertEqual(len(req['params']), 0)


    def test_response(self):
        res = jsonrpc_response('foo', 42)
        self.assertEqual(res['id'], 'foo')
        self.assertEqual(res['result'], 42)
        r = jsonrpc_result(res, None)
        self.assertEqual(r, 42)


    def test_response_compare(self):
        req = jsonrpc_request('foo_barBaz')
        res = jsonrpc_response(req['id'], 42)
        self.assertTrue(jsonrpc_is_response_to(req, res))


if __name__ == '__main__':
    unittest.main()
