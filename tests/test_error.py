# standard imports
import unittest
import json

# local imports
from jsonrpc_std.error import (
        JSONRPCException,
        JSONRPCCustomException,
        JSONRPCErrors,
        JSONRPCParseError,
        )
from jsonrpc_std.interface import (
        jsonrpc_error,
        DefaultErrorParser,
        )

class WrongError(Exception):
    pass


class RightError(JSONRPCCustomException):
    message = 'Right'
    code = -32001


class TestError(unittest.TestCase):

    def test_base(self):
        e = JSONRPCException('foo')
        self.assertEqual(str(e), 'Unknown error: foo')


    def test_error_by_code(self):
        e = JSONRPCErrors.get(-32700)
        self.assertTrue(isinstance(e, JSONRPCParseError))


    def test_custom_error(self):
        with self.assertRaises(KeyError):
            e = JSONRPCErrors.get(-1000)

        with self.assertRaises(ValueError):
            JSONRPCErrors.add(-32000, WrongError)

        with self.assertRaises(ValueError):
            JSONRPCErrors.add(JSONRPCErrors.local_min - 1, RightError)

        with self.assertRaises(ValueError):
            JSONRPCErrors.add(JSONRPCErrors.local_max + 1, RightError)

        JSONRPCErrors.add(-32000, RightError)
        e_retrieved = JSONRPCErrors.get(-32000, 'foo')
        self.assertEqual(type(RightError(None)), type(e_retrieved))
        self.assertEqual(str(e_retrieved), 'Right error: foo')


    def test_error_interface(self):
        uu = 'foo'
        e = jsonrpc_error(uu, -32700)
        self.assertEqual(e['error']['code'], -32700)
        self.assertEqual(e['error']['message'], 'Parse error')

      
    def test_default_error_translate(self):
        uu = 'foo'
        p = DefaultErrorParser()
        e = jsonrpc_error(uu, -32700)
        o = p.translate(e)
        print('e {}'.format(o))


    def test_error_serialize(self):
        e = JSONRPCParseError('foo', request_id='bar')
        o = dict(e)
        self.assertEqual(o['jsonrpc'], '2.0')
        self.assertEqual(o['id'], 'bar')
        self.assertEqual(o['error']['code'], -32700)
        self.assertEqual(o['error']['message'], 'Parse error: foo')


if __name__ == '__main__':
    unittest.main()
